/** @param {NS} ns **/
export async function main(ns) {
// 55k per 1GB RAM
// max RAM by default = getPurchasedServerMaxRam() = 1048576

// Buy 8GB ram servers
	await buyServer(8, ns.getPurchasedServers().length, ns);

	// upgrade 8GB servers to 512GB ones
	await buyServer(512, 0, ns);
// 56.32m per 1TB RAM
	// upgrade 512GB servers to 8192GB ones
	await buyServer(8192, 0, ns);
	// upgrade 8192GB servers to 131.07TB ones
	await buyServer(131072, 0, ns);
	// upgrade 131.07TB servers to 1.048PB ones (or whatever is max on the BN)
	await buyServer(ns.getPurchasedServerMaxRam(), 0, ns);
}

async function buyServer(ram, i, ns) {
	while (i < ns.getPurchasedServerLimit()) {
		if (ns.getPurchasedServers().length === ns.getPurchasedServerLimit() && ns.getServerMaxRam('home-' + i) >= ram) {
			++i;
			continue
		}
		if (ns.getServerMoneyAvailable('home') > ns.getPurchasedServerCost(ram)) {
			var hostname = 'home-' + i;
			if (ns.getPurchasedServers().length === ns.getPurchasedServerLimit()) {
				ns.killall(hostname);
				ns.deleteServer(hostname);
			}
			ns.purchaseServer(hostname, ram);
			ns.print('Hostname for new node will be: ' + hostname);
			await ns.scp ('bot.js', hostname);

			var serverRAM = ns.getServerMaxRam(hostname);
			var botRAMCost = ns.getScriptRam('bot.js');
			var threadCount = Math.floor(serverRAM/botRAMCost);
			ns.exec('bot.js', hostname, threadCount);

			++i;
		}
		await new Promise(r => setTimeout(r, 500));
	}
	ns.print('Past the ' + ram + 'GB section');
}
