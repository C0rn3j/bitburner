/** @param {NS} ns **/
export async function main(ns) {
	var victims =
	['n00dles',
		'zer0',
			'phantasy',
		'nectar-net',
			'silver-helix',
			'omega-net',
				'johnson-ortho',
				'avmnite-02h',
					'I.I.I.I',
	'foodnstuff',
	'sigma-cosmetics',
	'joesguns',
	'hong-fang-tea',
		'CSEC',
			'neo-net',
				'the-hub',
					'summit-uni',
						'millenium-fitness',
				'comptek',
					'catalyst',
				'netlink',
					'rothman-uni',
						'lexo-corp',
							'aerocorp',
								'deltaone',
									'univ-energy',
										'zb-def',
								'unitalife',
									'defcomm',
										'infocomm',
											'microdyne',
											'applied-energetics',
												'fulcrumtech',
													'.',
													'run4theh111z',
														'titan-labs',
															'helios',
																'omnitek',
																	'b-and-a',
																		'megacorp',
																	'clarkinc',
																		'ecorp',
																'4sigma',
																	'blade',
																		'The-Cave',
																			'w0r1d_d43m0n', // Only available after Red Pill is purchased?
																	'powerhouse-fitness',
																'kuai-gong',
																	'nwo',
																		'fulcrumassets',
												'stormtech',
												'vitalife',
									'zeus-med',
										'nova-med',
						'alpha-ent',
							'snap-fitness',
								'omnia',
									'icarus',
										'taiyang-digital',
									'solaris',
						'aevum-police',
							'global-pharm',
					'syscore',
				'crush-fitness',
					'zb-institute',
						'rho-construction',
							'galactic-cyber',
	'harakiri-sushi',
		'max-hardware',
	'iron-gym'
	];

	if (ns.serverExists('darkweb')) {
		victims.push('darkweb');
	} else {
		if (ns.getServerMoneyAvailable('home') >= 200000) {
			ns.purchaseTor();
			victims.push('darkweb');
		}
	}

	// Set number of ports we can open depending if we own their respective openers, and buy the programs we can
	var portOpenerMax = 0;
	if (ns.fileExists('BruteSSH.exe', 'home')) {
		++portOpenerMax;
	} else if (ns.getServerMoneyAvailable('home') >= 500000) {
		ns.purchaseProgram('BruteSSH.exe');
		++portOpenerMax;
	}
	if (ns.fileExists('FTPCrack.exe', 'home')) {
		++portOpenerMax;
	} else if (ns.getServerMoneyAvailable('home') >= 1500000) {
		ns.purchaseProgram('FTPCrack.exe');
		++portOpenerMax;
	}
	if (ns.fileExists('relaySMTP.exe', 'home')) {
		++portOpenerMax;
	} else if (ns.getServerMoneyAvailable('home') >= 5000000) {
		ns.purchaseProgram('relaySMTP.exe');
		++portOpenerMax;
	}
	if (ns.fileExists('HTTPWorm.exe', 'home')) {
		++portOpenerMax;
	} else if (ns.getServerMoneyAvailable('home') >= 30000000) {
		ns.purchaseProgram('HTTPWorm.exe');
		++portOpenerMax;
	}
	if (ns.fileExists('SQLInject.exe', 'home')) {
		++portOpenerMax;
	} else if (ns.getServerMoneyAvailable('home') >= 250000000) {
		ns.purchaseProgram('SQLInject.exe');
		++portOpenerMax;
	}

	// Buy niceties if we're above a certain money threshold
	if (! ns.fileExists('AutoLink.exe', 'home') && ns.getServerMoneyAvailable('home') >= 5000000) {
		ns.purchaseProgram('AutoLink.exe');
	}
	if (! ns.fileExists('DeepscanV1.exe', 'home') && ns.getServerMoneyAvailable('home') >= 3000000) {
		ns.purchaseProgram('DeepscanV1.exe');
	}
	if (! ns.fileExists('DeepscanV2.exe', 'home') && ns.getServerMoneyAvailable('home') >= 75000000) {
		ns.purchaseProgram('DeepscanV2.exe');
	}
	if (! ns.fileExists('ServerProfiler.exe', 'home') && ns.getServerMoneyAvailable('home') >= 5000000) {
		ns.purchaseProgram('ServerProfiler.exe');
	}
	if (! ns.fileExists('Formulas.exe', 'home') && ns.getServerMoneyAvailable('home') >= 250000000000) {
		ns.purchaseProgram('Formulas.exe');
	}

	var hackLevel = ns.getHackingLevel();

	for (var victim of victims) {
		// If we can't possibly hack the server, continue
		if (ns.getServerNumPortsRequired(victim) > portOpenerMax || ns.getServerRequiredHackingLevel(victim) > hackLevel) {
			ns.print('Cannot hack this target, continuing - ' + victim);
			continue;
		}
		ns.print('Processing ' + victim);
		if (ns.hasRootAccess(victim) === false) {
			if (ns.fileExists('BruteSSH.exe', 'home')) {
				ns.brutessh(victim);
			}
			if (ns.fileExists('FTPCrack.exe', 'home')) {
				ns.ftpcrack(victim);
			}
			if (ns.fileExists('relaySMTP.exe', 'home')) {
				ns.relaysmtp(victim);
			}
			if (ns.fileExists('HTTPWorm.exe', 'home')) {
				ns.httpworm(victim);
			}
			if (ns.fileExists('SQLInject.exe', 'home')) {
				ns.sqlinject(victim);
			}
			ns.nuke(victim);
		}

		if (ns.hasRootAccess(victim) === false) {
			ns.nuke(victim);
//			ns.connect(victim);
//			await ns.installBackdoor();
		}
		await ns.scp('bot.js', 'home', victim);
		ns.killall(victim);
		var serverRAM = ns.getServerMaxRam(victim);
		var botRAMCost = ns.getScriptRam('bot.js');
		var threadCount = Math.floor(serverRAM/botRAMCost);
		if (threadCount > 0) {
			ns.exec('bot.js', victim, threadCount);
		}
	}

	for (var victim of ns.getPurchasedServers()) {
		await ns.scp('bot.js', 'home', victim);
		ns.killall(victim);
		var serverRAM = ns.getServerMaxRam(victim);
		var botRAMCost = ns.getScriptRam('bot.js');
		var threadCount = Math.floor(serverRAM/botRAMCost);
		if (threadCount > 0) {
			ns.exec('bot.js', victim, threadCount);
		}
	}

	// Finally run bot on home
	victim = 'home';
	ns.kill('bot.js', victim);
	var serverRAM = ns.getServerMaxRam(victim);
	var botRAMCost = ns.getScriptRam('bot.js');
	var threadCount = Math.floor(serverRAM/botRAMCost);
	if (threadCount > 0) {
		ns.exec('bot.js', victim, threadCount - 10);
	}

	ns.disableLog('getServerRequiredHackingLevel');
	ns.disableLog('getHackingLevel');
	for (var victim of victims) {
		if (ns.getServerRequiredHackingLevel(victim) <= hackLevel) {
			var moneyAvail = ns.getServerMoneyAvailable(victim);
		}
//		var moneyMax = ns.getServerMaxMoney(victim);
//		var hackingLevel = ns.getServerRequiredHackingLevel(victim);
//		ns.print("Server " + victim + " has " + moneyAvail/1000000 + "b/" + moneyMax/1000000 + "b and requires hacking " + hackingLevel);
	}

}
