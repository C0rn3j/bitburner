/** @param {NS} ns **/
export async function main(ns) {
	ns.disableLog('getServerMoneyAvailable');
	ns.disableLog('gang.setMemberTask');
	//ns.disableLog('ALL');

	while (true) {
// https://www.reddit.com/r/Bitburner/comments/ble8lg/script_to_run_gang/

	// If we're not in a gang, try joining Slum Snakes. If it fails, sleep for 1s then retry
	if (!ns.gang.inGang()) {
		if (!ns.gang.createGang('Slum Snakes')) {
			await new Promise(r => setTimeout(r, 1000));
			continue;
		}
	}


	var members = ns.gang.getMemberNames();
	var newMemberName = (members.length+1).toString();
	if (ns.gang.canRecruitMember()) {
		ns.gang.recruitMember(newMemberName);
	}

//		var gangInfo = ns.gang.getGangInformation();
		for (let memberNum = 1; memberNum < members.length+1; memberNum++) {
			var member = memberNum.toString();
			// https://github.com/danielyxie/bitburner/blob/dev/markdown/bitburner.gangmemberascension.md
			var ascResult = ns.gang.getAscensionResult(member);
			// https://github.com/danielyxie/bitburner/blob/dev/markdown/bitburner.gangmemberinfo.md
			var memInfo = ns.gang.getMemberInformation(member);
			//			ns.gang.purchaseEquipment(i,);

//			ns.gang.setMemberTask(member, unassignedTask);

			var desirableAugs = [];

			var buyableEquipment = ns.gang.getEquipmentNames().filter(e => {
				return ns.gang.getEquipmentType(e) != "Augmentation" || desirableAugs.includes(e);
			});

			for(var e of buyableEquipment) {
//				if(memInfo.equipment.includes(e)) return;
//				if(memInfo.augmentations.includes(e)) return;

//				hadAll = false;

				if (ns.gang.getEquipmentCost(e) <= ns.getServerMoneyAvailable('home')) {
					ns.gang.purchaseEquipment(member, e);
				}
			}

			// Set tasks
			// Respect required table
			// 1,2,3:      0
			// 4:          5
			// 5:         25
			// 6:        125
			// 7:        625
			// 8:       3125
			// 9:     15 625
			// 10:    78 125
			// 11:   390 625
			// 12: 1 953 125

			if (members.length < 5 && memInfo['str'] > 100) {
				ns.gang.setMemberTask(member, 'Mug People');
				continue
			}
			if (members.length < 6 && memInfo['str'] > 300) {
				ns.gang.setMemberTask(member, 'Terrorism');
				continue
			}
			if (members.length < 7 && memInfo['str'] > 300) {
				ns.gang.setMemberTask(member, 'Terrorism');
				continue
			}
			if (members.length < 8 && memInfo['str'] > 300) {
				ns.gang.setMemberTask(member, 'Terrorism');
				continue
			}
			if (members.length < 9 && memInfo['str'] > 500) {
				ns.gang.setMemberTask(member, 'Terrorism');
				continue
			}
			if (members.length < 10 && memInfo['str'] > 1000) {
				ns.gang.setMemberTask(member, 'Terrorism');
				continue
			}
			if (members.length < 11 && memInfo['str'] > 10000) {
				ns.gang.setMemberTask(member, 'Terrorism');
				continue
			}
			if (members.length < 12 && memInfo['str'] > 20000) {
				ns.gang.setMemberTask(member, 'Terrorism');
				continue
			}


			if (memberNum === 1 && memInfo['str_asc_mult'] > 20) {
				ns.gang.setMemberTask(member, 'Terrorism');
				continue
			}
			if (memberNum === 2 && memInfo['str_asc_mult'] > 20) {
				ns.gang.setMemberTask(member, 'Human Trafficking');
				continue
			}
			// If member can ascend, ascend them if their str mult is more than 30%
			if (ascResult != null) {
				if (ascResult['str'] > 1.25) {
					ns.gang.ascendMember(member);
				}
			}
			ns.gang.setMemberTask(member, 'Train Combat');

		}
		await new Promise(r => setTimeout(r, 1000));
	}
}
