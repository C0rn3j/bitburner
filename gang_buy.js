/** @param {NS} ns **/
export async function main(ns) {
	ns.disableLog('getServerMoneyAvailable');
	ns.disableLog('gang.setMemberTask');
	//ns.disableLog('ALL');

	while (true) {
// https://www.reddit.com/r/Bitburner/comments/ble8lg/script_to_run_gang/
	// Respect required table
	// 1,2,3: 0 | 4: 5
	// 5: 25    | 6: 125
	// 7:       625
	// 8:       3125
	// 9:     15 625
	// 10:    78 125
	// 11:   390 625
	// 12: 1 953 125


	// Join Slum Snakes if available
//	 if (! ns.gang.inGang()) {
//
//	}

	var members = ns.gang.getMemberNames();
	var newMemberName = (members.length+1).toString();
	if (ns.gang.canRecruitMember()) {
		ns.gang.recruitMember(newMemberName);
	}

//		var gangInfo = ns.gang.getGangInformation();
		for (let memberNum = 1; memberNum < members.length+1; memberNum++) {
			var member = memberNum.toString();
			var ascResult = ns.gang.getAscensionResult(member);
				// "hack":1,
				// "str":2.5434565871760793,
				// "def":2.758757649289257,
				// "dex":2.2914935604821247,
				// "agi":2.396152626541528,
				// "cha":1
			var memInfo = ns.gang.getMemberInformation(member);
				// "hack_asc_mult":1,
				// "str_asc_mult":1,
				// "def_asc_mult":1,
				// "dex_asc_mult":1,
				// "agi_asc_mult":1,
				// "cha_asc_mult":1
			//			ns.gang.purchaseEquipment(i,);

//			ns.gang.setMemberTask(member, unassignedTask);

			var desirableAugs = [];

			var buyableEquipment = ns.gang.getEquipmentNames().filter(e => {
				return ns.gang.getEquipmentType(e) != "Augmentation" || desirableAugs.includes(e);
			});

			for(var e of buyableEquipment) {
//				if(memInfo.equipment.includes(e)) return;
//				if(memInfo.augmentations.includes(e)) return;

//				hadAll = false;

				if (ns.gang.getEquipmentCost(e) <= ns.getServerMoneyAvailable('home')) {
					ns.gang.purchaseEquipment(member, e);
				}
			}

		}
		await new Promise(r => setTimeout(r, 1000));
	}
}
