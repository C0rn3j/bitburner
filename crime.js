/** @param {NS} ns **/
export async function main(ns) {
	var gangExists = ns.gang.inGang();
	while (gangExists == false) {
		if (!ns.isBusy()) {
			ns.commitCrime('Homicide');
		}
		// If we're not in a gang, try joining one and if it works, run gang.js
		if (!ns.gang.inGang()) {
			if (ns.gang.createGang('Slum Snakes')) {
				ns.exec('gang.js', 'home', 1);
				gangExists = true;
			}
		}
		await new Promise(r => setTimeout(r, 100));
	}
}
