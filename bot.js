/** @param {NS} ns **/
export async function main(ns) {
	var firstVictim  = 'foodnstuff';   //     0
	var secondVictim = 'max-hardware'; //    80
	var thirdVictim  = 'silver-helix'; //   150
	var fourthVictim = 'silver-helix'; //   150
//	var fourthVictim = 'aevum-police'; //   429
	var fifthVictim  = 'megacorp';     // 1,110
	// TEMP
//	if (ns.getHackingLevel() >= 150) {
//		secondVictim = 'nein';
//	}
//	thirdVictim = 'max-hardware';

	var victim = firstVictim;
	if        (ns.getHackingLevel() >= 1110 && ns.hasRootAccess(fifthVictim)) {
		victim = fifthVictim;
	} else if (ns.getHackingLevel() >=  429 && ns.hasRootAccess(fourthVictim)) {
		victim = fourthVictim;
	} else if (ns.getHackingLevel() >=  150 && ns.hasRootAccess(thirdVictim)) {
		victim = thirdVictim;
	} else if (ns.getHackingLevel() >=   80 && ns.hasRootAccess(secondVictim)) {
		victim = secondVictim;
	}

	var moneyThresh = ns.getServerMaxMoney(victim) * 0.75;
	var securityThresh = ns.getServerMinSecurityLevel(victim);

	while (true) {
		// TODO think of detection for Xth ascension
//		if (victim === firstVictim || victim === secondVictim) {
//			await ns.hack(victim);
//			continue
//		}
		if (ns.getServerSecurityLevel(victim) > securityThresh) {
			await ns.weaken(victim);
		} else if (ns.getServerMoneyAvailable(victim) < moneyThresh) {
			ns.print('Server max money: ' + moneyThresh + '; Current money: ' + ns.getServerMoneyAvailable(victim));
			await ns.grow(victim);
		} else {
			await ns.hack(victim);
		}
	}
}
